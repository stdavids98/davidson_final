﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

//	Variables that reference GameObjects
	public GameObject shotLeft;
	public Transform shotSpawn; 

//	Variables to controll the bullets
	public float fireRate;
	public float delay;
	[HideInInspector] public bool gameOver;

//	As soon as this object is spawned, it will begin firing
	void Start () 
	{

		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Update()
	{
		GameObject gui = GameObject.Find ("GUI");
		GUIController guiControl = gui.GetComponent<GUIController> ();
		gameOver = guiControl.gameOver;	
		GameObject enemy = GameObject.FindWithTag ("Enemy");
		if (gameOver) 
		{
			Destroy (enemy);
		}
	}
//	A function that allows the object to shoot
	void Fire ()
	{
		SoundManagerScript.PlaySound ("fire");
		Instantiate (shotLeft, shotSpawn.position, shotSpawn.rotation);
	}
}
