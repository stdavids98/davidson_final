﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

	public static AudioClip playerHitSound, FireSound, playerDeathSound, enemyDeathSound, healthSound, ammoSound;
	static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
		playerHitSound = Resources.Load<AudioClip> ("playerHit");
		FireSound = Resources.Load<AudioClip> ("shot");
		playerDeathSound = Resources.Load<AudioClip> ("horseDeath");
		enemyDeathSound = Resources.Load<AudioClip> ("enemyDeath");
		healthSound = Resources.Load<AudioClip> ("pickup");
		ammoSound = Resources.Load<AudioClip> ("pickup2");

		audioSrc = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void PlaySound (string clip)
	{
		switch (clip) 
		{
		case "fire":
			audioSrc.PlayOneShot (FireSound);
			break;
		case "playerHit":
			audioSrc.PlayOneShot (playerHitSound);
			break;
		case "playerDeath":
			audioSrc.PlayOneShot (playerDeathSound);
			break;
		case "health":
			audioSrc.PlayOneShot (healthSound);
			break;
		case "ammo":
			audioSrc.PlayOneShot (ammoSound);
			break;
		case "enemyDeath":
			audioSrc.PlayOneShot (enemyDeathSound);
			break;
		}
	}
}
