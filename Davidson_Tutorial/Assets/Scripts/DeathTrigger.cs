﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour {

//  If the player enters this TriggerBox, the game is reloaded
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			GameObject gui = GameObject.Find ("GUI");
			GUIController guiControl = gui.GetComponent<GUIController> ();
			guiControl.deathText.text = "You jumped off a cliff!!";
			guiControl.GameOver ();

		}
	}
}
