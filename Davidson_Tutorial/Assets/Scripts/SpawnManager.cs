﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

//	Variables referencing GameObjects
	public GameObject platform;
	public GameObject specPlat;

//	Variables to determine how many platforms are to spawn, and where they can spawn.
	public int maxPlatforms = 3;
	public float verticalMin = -2f;
	public float verticalMax = 2f;

//	Variable for a random position
	new Vector2 randomPosition;

//	Variable for the original position
	private Vector2 originPosition;

	void Start () 
	{
//		Makes the originPosition the position of the first platform
		originPosition = transform.position;

//		Calls the Spawn functions
		Spawn ();
	}


//	A function to spawn more platforms
	public void Spawn () 
	{
//		Spawns as many platforms as I want, based on the var maxPlatforms
		for (int i = 0; i < maxPlatforms; i++) 
		{
//			Creats a random position with the constraints listed above
			randomPosition = originPosition + new Vector2 (10, Random.Range(verticalMin, verticalMax));

//			Spawn the platform in the position create above
			GameObject plat = Instantiate(platform, randomPosition, Quaternion.identity) as GameObject;

//			Sets the originPosition to the positon of the last platform
			originPosition = randomPosition;
//			plat.GetComponent<DestroyMe> ().Invoke ("destroyMe", i*2 + 10f);
		}

//		Spawn a special platform that will allow more platforms to be spawned
		Instantiate(specPlat, originPosition + new Vector2 (10,0), Quaternion.identity);

//		Adds to the originPositions x value so the next platform to spawn won't spawn on top of the special platform
		originPosition.x += 10;
	}


}


//plat.GetComponent<DestroyMe> ().Invoke ("destroyMe", i*2 + 10f);