﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour {

//	Variables to refernce GameObjects
	public Transform[] objectSpawns;
	public GameObject enemy;

//	Variable to hold a random value later
	float objectChoose;

	void Start ()
	{
//		Calls the Spawn funciton
		Spawn ();
	}

//	Function that spawns objects on the platforms
	void Spawn()
	{
//		Creates a random float between 0 and 1
		objectChoose = Random.value;

//		If the random variable is greater than a certain amount, an enemy will spawn
		if (objectChoose > 0.5f) 
		{
			for (int i = 0; i < 2; i++) 
			{
				float coinFlip = Random.value;
				if (coinFlip > 0.4f) 
				{
					Instantiate (enemy, objectSpawns [i].position, Quaternion.identity);
				}
			}
		}


	}

}
