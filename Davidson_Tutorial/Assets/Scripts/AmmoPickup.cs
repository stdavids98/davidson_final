﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour {
	
	//	Variable that assigns a value to the objects
	public int ammoValue;

	//	Variable to check if the object has been collected yet
	bool collected = false;

	//	If the player enters the Object's TriggerBox, the Object will be destroyed.
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			if (!collected) 
			{
				collected = true;
				SoundManagerScript.PlaySound ("ammo");
				Destroy (gameObject);

				GameObject gui = GameObject.Find ("GUI");
				GUIController guiControl = gui.GetComponent<GUIController> ();
				guiControl.AddAmmo (ammoValue);
			}
		}
	}

}
