﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Mover : MonoBehaviour 
{
//	Variable to hold the Rigidbody component
	Rigidbody2D rb2d;

//	Variables to control the moving object
	public float velX = 12.5f;
	float velY = 0f;

	void Start ()
	{
//		Get's Rigidbody
		rb2d = GetComponent<Rigidbody2D> ();
	}

	void Update () 
	{
//		Moves Object
		rb2d.velocity = new Vector2 (velX, velY);
	}

}
