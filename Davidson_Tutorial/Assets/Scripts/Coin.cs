﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

//	Variable that assigns a value to the objects
	public int healthValue;

//	Variable to check if the object has been collected yet
	bool collected = false;

//	If the player enters the Object's TriggerBox, the Object will be destroyed.
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			if (!collected) 
			{
				collected = true;
				SoundManagerScript.PlaySound ("health");
				Destroy (gameObject);

				GameObject gui = GameObject.Find ("GUI");
				GUIController guiControl = gui.GetComponent<GUIController> ();
				guiControl.UpdateHealth (healthValue);
			}
		}
	}

}
