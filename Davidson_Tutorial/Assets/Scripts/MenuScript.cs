﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour {

	public void StartGame()
	{
		Application.LoadLevel ("Davidson_Tutorial");
	}
	public void OpenStory()
	{
		Application.LoadLevel ("Story_Scene");
	}
	public void OpenInstruct()
	{
		Application.LoadLevel ("Instructions_Scene");
	}
	public void QuitGame()
	{
		Application.Quit ();
	}
	public void Return()
	{
		Application.LoadLevel ("Title_Screen");
	}
}
