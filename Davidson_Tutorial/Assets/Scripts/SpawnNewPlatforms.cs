﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNewPlatforms : MonoBehaviour {

//	Variable to check if the player has entered the TriggerBox later
//	Needed so the TriggerBox isn't activated more than once
	bool entered = false;

//	If the player enters this TriggerBox, new platforms will spawn
	void OnTriggerEnter2D(Collider2D other)
	{
//		Checks to make sure the other object is the Player
		if(other.CompareTag("Player"))
		{
//			Checks to see if it has be activated yet
			if (!entered) 
			{
//				Says it's been activated
				entered = true;

//				Finds the SpawnManager
				GameObject sm = GameObject.FindGameObjectWithTag ("SpawnManager");

//				Uses the Spawn function in the Spawn Manager
				sm.GetComponent<SpawnManager> ().Spawn ();
			}
		}

	}
}
