﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {

//	Variables that reference GameObjects
	public Text scoreText;
	public Text highText;
	public Text restartText;
	public Text gameOverText;
	public Text healthText;
	public Text ammoText;
	public Text deathText;
	public Text quitText;



//	Variables to store the player's scores
	private int score;
	int highScore = 0;

//	Variables for Game Over condition
	[HideInInspector] public bool gameOver;
	private bool restart;

//	Variables for Health
	public int health;

// Variables for Ammo
	public int ammo;

	void Start ()
	{
// 		Code for Score Text
		score = 0;
		UpdateScore ();

//		Code for HighScore Text
		highScore = PlayerPrefs.GetInt ("HighScore", 0);
		UpdateHighScore ();

//		Code for Game Over
		restartText.text = "";
		quitText.text = "";
		gameOverText.text = "";
		deathText.text = "";

//		Code for Health
		healthText.text = "Health: " + (health + 1);

//		Code for Ammo
		ammoText.text = "Ammo: " + (ammo + 1);
	}

	void Update ()
	{
//		If the player gets a game over, they must press 'R' to restart the game
		if (gameOver) 
		{
			restartText.text = "Press 'R' for Restart";
			quitText.text = "Press 'Q' for the Title Screen";
			restart = true;
			if (Input.GetKeyDown (KeyCode.R)) 
			{
				gameOver = false;
				Application.LoadLevel (Application.loadedLevel);
			}
			if (Input.GetKeyDown (KeyCode.Q)) 
			{
				gameOver = false;
				Application.LoadLevel ("Title_Screen");
			}
		}

	}

//***		CODE FOR SCORE KEEPING		***
//	A function to add to the players score
	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

//	A function to update to the players score
	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;
	}

//	A function to update to the players high score
	void UpdateHighScore()
	{
		highText.text = "HighScore: " + highScore;
	}

//***		CODE FOR GAME OVER			***
//	A function to let the player know they got a Game Over
	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		Destroy (player);
		if (score > highScore)
		{
			highScore = score;
			PlayerPrefs.SetInt ("HighScore", highScore);
		}
	}

//***		CODE FOR HEALTH				***
//	A function to update the players health
	public void UpdateHealth(int healthAdd)
	{
		health += healthAdd;
		healthText.text = "Health: " + (health + 1) ;
	}

//	A function that kills the player if they run out of health
	public void BloodLoss()
	{
		if (health == 0) 
		{
			healthText.text = "Health: 0";
			deathText.text = "You ran out of Health!";
			GameOver ();

		}
	}

//***		CODE FOR AMMO				***
//	A function for the player to use their ammo
	public void UseAmmo()
	{
		if (ammo > 0) 
		{
			ammo -= 1;
			UpdateAmmo ();
		}
	}

//	A function to update the players ammo count
	public void UpdateAmmo()
	{
		ammoText.text = "Ammo: " + (ammo + 1);
	}

//	A function that ends the game if the player runs out of ammo
	public void OutOfAmmo ()
	{
		if (ammo == 0) 
		{
			deathText.text = "You ran out of ammo!";
			ammoText.text = "Ammo: 0";
			GameOver ();
		}
	}

//	A function that allows the player to recieve more ammo
	public void AddAmmo(int ammoCount)
	{
		ammo += ammoCount;
		ammoText.text = "Ammo: " + (ammo + 1);
	}

}
