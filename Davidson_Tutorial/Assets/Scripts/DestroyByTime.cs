﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour 
{
//	Variable to hold the Lifetime
	public float lifetime;

	void Start ()
	{
//		After this object has spawned, it will despawn after the given amount of time
		Destroy (gameObject, lifetime);
	}
}
