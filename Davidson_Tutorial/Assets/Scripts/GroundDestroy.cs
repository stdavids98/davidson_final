﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDestroy : MonoBehaviour {

//	Variable to check if the platform has been touched
	private bool passed = false;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
//			If the platform has been touched by the player, DestroyGround will be called
			passed = true;
			StartCoroutine (DestroyGround ());
		}
	}

//	A function to destory the platform after a given amount of time.
	IEnumerator DestroyGround()
	{
		if (passed) 
		{
			yield return new WaitForSeconds (15);
			Destroy (gameObject);
		}
	}
}
