﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotController : MonoBehaviour {

//	Variables refernce GameObjects
	public GameObject shotRight;
	public GameObject shotLeft;
	public Transform shotSpawn;

//	Variables to control bullets
	public float fireRate;
	private float nextFire;
	[HideInInspector] public bool faceRight;

	void Update () 
	{
//		A bunch of code that's needed to references the SimplePlatformController script
		GameObject Hero = GameObject.Find ("hero");
		SimplePlatformController platControl = Hero.GetComponent<SimplePlatformController> ();
		faceRight = platControl.facingRight;

//		Allows player to fire bullets
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			GameObject gui = GameObject.Find ("GUI");
			GUIController guiControl = gui.GetComponent<GUIController> ();
			SoundManagerScript.PlaySound ("fire");
			guiControl.UseAmmo ();

			if (guiControl.ammo > 0) 
			{
				if (faceRight) 
				{
					nextFire = Time.time + fireRate;
					Instantiate (shotRight, shotSpawn.position, shotSpawn.rotation);
				}
				if (!faceRight) 
				{
					nextFire = Time.time + fireRate;
					Instantiate (shotLeft, shotSpawn.position, shotSpawn.rotation);
				}
			} 
			if (guiControl.ammo == 0) 
			{
				guiControl.OutOfAmmo ();
			} 
		}
	}
}
