﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour 
{
//	Variables for GameObjects
	private Rigidbody2D rb;
	public GameObject hp;
	public GameObject ap;

//	Variable that assigns a value to enemies
	public int scoreValue;

//	public GameObject pickupPrefab;

	void Start () 
	{
//		Getting the Rigidbody
		rb = GetComponent<Rigidbody2D>();
	}

//	Activates when an objects enters the TriggerBox
	void OnTriggerEnter2D(Collider2D other)
	{
//		Checks the tag of the object.  If it's any one of these, the script won't do anything.
		if (other.CompareTag  ("ground") || other.CompareTag ("EnemyBullet") || other.CompareTag ("Enemy") || other.CompareTag ("Coin")) 
		{
			return;
		}

		if (CompareTag ("EnemyBullet") && other.CompareTag ("ground")) 
		{
			Destroy (gameObject);
		}

//		Checks the tag of the object.  If it's the player, the game is restarted.
		if (other.CompareTag ("Player")) {
			GameObject guideath = GameObject.Find ("GUI");
			GUIController guideathControl = guideath.GetComponent<GUIController> ();
			if (guideathControl.health >= 1) 
			{
				guideathControl.UpdateHealth (-1);
				SoundManagerScript.PlaySound ("playerHit");
			} 
			if  (guideathControl.health == 0)
				
			{
				Destroy (other.gameObject);
				guideathControl.BloodLoss ();
				SoundManagerScript.PlaySound ("playerDeath");
			}
		}
			
//		If the object isn't a tag listed, both objects will be destoyed.
		else 
		{
			Destroy (other.gameObject);
			SoundManagerScript.PlaySound ("enemyDeath");

		}
		Destroy (gameObject);


//		Code needed to add the value of the destroyed object to the player's score
		GameObject gui = GameObject.Find ("GUI");
		GUIController guiControl = gui.GetComponent<GUIController> ();
		guiControl.AddScore (scoreValue);

		if (CompareTag  ("Enemy"))
		{
			int coinFlip = Random.Range (0, 2);
			float objectChoose = Random.value;
			if (coinFlip > 0)
			{
				if (objectChoose > 0.35f) 
				{
					Instantiate (hp, transform.position, hp.transform.rotation);
				}
				if (objectChoose < 0.35f) 
				{
					Instantiate (ap, transform.position, hp.transform.rotation);
				}
			}
		}

		

	}
}